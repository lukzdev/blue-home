﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpotsManager : MonoBehaviour
{
    public List<GameObject> spots;

    public List<Sprite> terrainSprites;


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < spots.Count; i++)
        {
            spots[i].GetComponent<SpriteRenderer>().sprite = terrainSprites[Random.Range(0, terrainSprites.Count - 1)];
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
