﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Cloud : MonoBehaviour
{

    public SpriteRenderer sprite;

    public List<Sprite> sprites;
    public float degPerSecMin = -5.0f;
    public float degPerSecMax = 5.0f;
    private float degPerSec = 1;

    void Start()
    {
        float angle = Random.Range(0, 360);
        transform.Rotate(0, 0, angle);
        Vector3 pos = Utils.Vector3(sprite.transform.localPosition);
        pos.y += Random.Range(0f, 1f);
        sprite.transform.localPosition = pos;
        degPerSec = Random.Range(degPerSecMin, degPerSecMax);

        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
        RestartFade(Random.Range(0f, 1f));

        sprite.sprite = sprites[Random.Range(0, sprites.Count)];
    }

    void RestartFade (float initialPosition = 0)
    {
        Sequence tween = DOTween.Sequence()
            .AppendInterval(Random.Range(5f, 10f))
            .Append(sprite.DOFade(1, Random.Range(2.5f, 5f)).SetEase(Ease.InOutSine))
            .AppendInterval(Random.Range(5f, 10f))
            .Append(sprite.DOFade(0, Random.Range(2.5f, 5f)).SetEase(Ease.InOutSine))
            .OnComplete(() => RestartFade());

        if (initialPosition > 0) 
        {
            tween.fullPosition = tween.Duration() * initialPosition;
        }
        
    }

    void Update()
    {
        transform.Rotate(0, 0, degPerSec * Time.deltaTime);        
    }
}
