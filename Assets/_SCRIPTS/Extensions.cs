﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static Vector3 Set(this Vector3 that, Vector3 other)
    {
        that.x = other.x;
        that.y = other.y;
        that.z = other.z;
        return that;
    }
}
