﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    public static Vector3 Vector3(Vector3 other) 
    {
        Vector3 result = new Vector3();
        result.x = other.x;
        result.y = other.y;
        result.z = other.z;
        return result;
    }
}
