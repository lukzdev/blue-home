﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Hazard : MonoBehaviour, IPointerClickHandler
{
    public GameObject root;

    public GameObject completeFX;

    public SpriteRenderer sprite;
    public SpriteRenderer progress;

    public Color progressDefault;
    public Color progressWarning;
    public float progressWarningPercent = .3f;
    public float progressHalfWidth = .07f;

    public bool destroyOnComplate = false;

    public float progressPerTap = -.5f;

    public float initialProgress = 0f;
    public float totalProgress = 10f;
    public float progressPerSecond = .1f;
    public float CurrentProgress {get; private set;}

    public float CurrentProgress01 {get; private set;}

    public int extraScore = 10;

    public delegate void OnCompleteHandler(Hazard hazard);

    public event OnCompleteHandler OnComplete;

    public delegate void OnFullHandler(Hazard hazard);

    public event OnFullHandler OnFull;

    public GameObject repairFx;

    void Start()
    {
        // root.transform.Rotate(0, 0, Random.Range(0, 360));
        Restart();
    }

    public void Restart ()
    {
        CurrentProgress = initialProgress;
    }

    void Update()
    {
        CurrentProgress += progressPerSecond * Time.deltaTime;
        if (CurrentProgress > totalProgress) {
            CurrentProgress = totalProgress;
            if (OnFull != null) OnFull.Invoke(this);
        }
        float a = Mathf.Clamp01(CurrentProgress/totalProgress);
        CurrentProgress01 = a;
        progress.transform.localScale = new Vector3(a, 1, 1);
        Vector3 lp = progress.transform.localPosition;
        progress.transform.localPosition = new Vector3(-progressHalfWidth + a * progressHalfWidth, lp.y, lp.z);
        
        if (a > progressWarningPercent) 
        {
            progress.color = progressWarning;
        } 
        else
        {
            progress.color = progressDefault;
        }
    }

    public void OnPointerClick(PointerEventData eventData) 
    {

        Instantiate(repairFx, transform.position, transform.rotation, root.transform.parent);

        GameManager.ScreenShake();
        
        // Debug.Log("click");
        CurrentProgress += progressPerTap;
        if (CurrentProgress <= 0) {
            CurrentProgress = 0;
            if (completeFX != null)
            {
                Instantiate(completeFX, transform.position, transform.rotation, root.transform.parent);
                // Instantiate(completeFX, transform.position, transform.rotation);
            }
            Complete();
            if (destroyOnComplate) 
            {
                GameManager.ScreenShake(0.16f);
                Destroy();
            }
        }
    }

    public void Complete () 
    {
        if (OnComplete != null) OnComplete.Invoke(this);
    }

    public void Full () 
    {
        if (OnFull != null) OnFull.Invoke(this);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        
    }

    public void Destroy() 
    {
        Destroy(root);
    }
}
