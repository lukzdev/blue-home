﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sounds : MonoBehaviour
{
    protected static Sounds instance;

    public AudioSource music;
    public AudioSource gameOver;
    public AudioSource hazardFixed;
    public AudioSource meteoriteExploded;
    public AudioSource pop;
    
    void Awake()
    {
        instance = this;
    }

    static void Toggle (AudioSource audio, bool enabled)
    {
        if (audio == null) return;
        if (enabled)
        {
            audio.Play();
        }
        else
        {
            audio.Pause();
        }
    }

    public static void PlayMusic (bool enabled) 
    {
        if (instance == null) return;
        Toggle(instance.music, enabled);
    }

    public static void PlayGameOver () 
    {
        if (instance == null) return;
        Toggle(instance.gameOver, true);
    }

    public static void PlayHazardFixed () 
    {
        if (instance == null) return;
        Toggle(instance.hazardFixed, true);
    }

    public static void PlayMeteoriteExploded () 
    {
        if (instance == null) return;
        Toggle(instance.meteoriteExploded, true);
    }

    public static void PlayPop () 
    {
        if (instance == null) return;
        Toggle(instance.pop, true);
    }
}
