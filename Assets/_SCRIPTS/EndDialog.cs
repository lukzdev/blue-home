﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class EndDialog : MonoBehaviour, IPointerClickHandler
{
    public GameManager gm;
    public TextMeshPro score;
    public float changeDelay = 2;
    private float remainingDelay;
    void Start()
    {
        score.text = "Score " + gm.Score;
        Sounds.PlayGameOver();
        remainingDelay = changeDelay;
    }

    void Update()
    {
        remainingDelay -= Time.deltaTime;
    }   

    public void OnPointerClick(PointerEventData eventData) 
    {
        if (remainingDelay <= 0)
        {
            remainingDelay = changeDelay;
            gm.Restart();
        }
    }
}
