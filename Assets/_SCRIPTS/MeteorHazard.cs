﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class MeteorHazard : MonoBehaviour
{
    bool falling = false;

    public GameObject root;

    public SpriteRenderer sprite;
    public GameObject groundFX;

    public Hazard hazard;

    public float minFallDelay = 10;
    public float maxFallDelay = 15;
    private float angle = 0;
    private float angleVel = 0;
    void Start()
    {
        DOTween.Sequence()
            .AppendInterval(Random.Range(minFallDelay, maxFallDelay))
            .OnComplete(() => {
                // we dont cancel this so do that
                if (this != null) Fall();
            });
        angle = Random.Range(0, 360);
        sprite.transform.rotation = Quaternion.Euler(0, 0, angle);
        angleVel = Random.Range(-45, 45);

        hazard.OnComplete += OnComplete;
    }

    void OnComplete(Hazard hazard)
    {
        Sounds.PlayPop();
    }
    
    void Update()
    {
        if (!falling && sprite.isVisible) 
        {
            Fall();
        }
        angle += angleVel * Time.deltaTime;
        sprite.transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    void Fall () {
        if (falling) 
        {
            return;
        }
        Debug.Log("Failling " + name);
        falling = true;
        // straigh down, a bit side to side
        transform
            .DOLocalMove(new Vector3(Random.Range(-1f, 1f), 0, 0), 60f);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Planet"))
        {
            Debug.Log("Nuke! ");
            Instantiate(groundFX, transform.position, transform.rotation, root.transform.parent);
            Sounds.PlayPop();
            hazard.Full();
            hazard.Destroy();
        }
    }
}
