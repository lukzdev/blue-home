﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OilSpillHazard : MonoBehaviour
{
    public Hazard hazard;

    void Start()
    {
        hazard.OnComplete += OnComplete;    
    }

    void OnComplete (Hazard hazard)
    {
        Sounds.PlayMeteoriteExploded();
    }
}
