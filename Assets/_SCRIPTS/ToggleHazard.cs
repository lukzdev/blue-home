﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleHazard : MonoBehaviour
{
    public string hazardName;
    public Hazard hazard;
    public GameObject[] toggleObjects;

    public SpriteRenderer sprite;
    public Sprite hazardOn;
    public Sprite hazardOff;

    public float hazardOnPercent = .3f;

    public bool HazardEnabled {get; private set;}

    void Start()
    {
        hazard.OnComplete += OnComplete;
        EnableHazard(hazard.CurrentProgress01 >= hazardOnPercent, false);
    }

    void OnComplete (Hazard hazard) 
    {
        // EnableHazard(false);
    }

    void EnableHazard(bool enabled, bool sound = true)
    {
        foreach (var item in toggleObjects)
        {
            if (item != null)
            {
                item.SetActive(enabled);
            }
        }
        HazardEnabled = enabled;
        if (enabled)
        {
            sprite.sprite = hazardOn;
        }
        else
        {
            sprite.sprite = hazardOff;
            if (sound) Sounds.PlayMeteoriteExploded();
        }
    }

    void Update()
    {
        bool turnOn = hazard.CurrentProgress01 >= hazardOnPercent;
        if (turnOn != HazardEnabled)
        {
            EnableHazard(turnOn);
        }
    }
}
