﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragHandler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler
{

    private Camera mainCamera;

    public GameObject target;

    public float speed = 8f;
    public float damping = .95f;

    private float velocity;

    void Awake () {
        // Camera.main does FindGameObjectsWithTag and doesnt change so we can cache it
        mainCamera = Camera.main;
    }

    void Update()
    {
        float zeroVel = .05f;
        if (velocity >= zeroVel || velocity <= -zeroVel) {
            target.transform.Rotate(0, 0, velocity);
            velocity *= damping;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Ignored(eventData)) return;
        velocity = 0;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Ignored(eventData)) return;
        // we could save offset from center if we feel like
        Vector3 pos = ScreenToWorldPoint(eventData);
        // Debug.Log("drag start " + pos);
    }
    public void OnDrag(PointerEventData eventData)
    {
        if (Ignored(eventData)) return;
        Vector3 prev = ScreenToWorldPoint(eventData.position - eventData.delta);
        Vector3 pos = ScreenToWorldPoint(eventData);
        Vector3 delta = prev - pos;
        velocity = delta.x * speed;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (Ignored(eventData)) return;
        Vector3 pos = ScreenToWorldPoint(eventData);
        // Debug.Log("drag end " + pos);
    }
    private Vector3 ScreenToWorldPoint(PointerEventData eventData) {
        return ScreenToWorldPoint(eventData.position);
    }

    private Vector3 ScreenToWorldPoint(Vector3 position) {
        // we want the position at the camera plane (z=10), but we want to spawn stuff at z=0
        Vector3 wp = mainCamera.ScreenToWorldPoint(new Vector3(position.x, position.y, 10));
        wp.z = 0;
        return wp;
    }

    private bool Ignored (PointerEventData eventData) {
        // ignore if not left mouse or first touch
        // https://docs.unity3d.com/ScriptReference/EventSystems.PointerEventData-pointerId.html
        // return !(eventData.pointerId == -1 || eventData.pointerId == 0);
        return false;
    }
}
