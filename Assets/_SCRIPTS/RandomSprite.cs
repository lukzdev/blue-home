﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSprite : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;

    public Sprite[] spritess;

    void Start()
    {
        spriteRenderer.sprite = spritess[Random.Range(0, spritess.Length)];   
    }
}
