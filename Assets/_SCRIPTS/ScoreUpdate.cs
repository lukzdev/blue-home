﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreUpdate : MonoBehaviour
{
    public GameManager gm;
    public TextMeshPro text;

    private int score = 0;
    void Start()
    {
        score = gm.Score;
        UpdateText();
    }

    // Update is called once per frame
    void Update()
    {
        if (score != gm.Score) 
        {
            score = gm.Score;
            UpdateText();
        }
    }

    void UpdateText()
    {
        text.text = score.ToString();
    }
}
