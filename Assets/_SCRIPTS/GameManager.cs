﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour
{
    public int Score {get; private set;}

    public GameObject endDialog;
    public GameObject scoreDisplay;
    public HazardSpawner hazardSpawner;

    public float scorePerSecond = .5f;
    private float score;

    [SerializeField]
    public static float shakePower = 0;

    void Awake()
    {
        Score = 0;
    }

    void Start()
    {
        DOTween.Init(true, true, LogBehaviour.Verbose);
        Restart();
    }

    void Update()
    {
        score += scorePerSecond * Time.deltaTime;
        Score = Mathf.RoundToInt(score);

        ShakeScreen();
    }

    public void AddScore (int extra) 
    {
        score += extra;
    }

    public void GameOver ()
    {
        endDialog.SetActive(true);
        scoreDisplay.SetActive(false);

        GameManager.ScreenShake(0.15f);
    }

    public void Restart () 
    {
        Score = 0;
        score = 0;
        endDialog.SetActive(false);
        scoreDisplay.SetActive(true);
        hazardSpawner.Restart();
    }

    public static void ScreenShake(float amount = 0.08f)
    {
        shakePower = amount;
    }

    void ShakeScreen()
    {
        if (shakePower > 0.01f)
        {
            shakePower *= 0.65f;

            if (Random.Range(0, 1) > 0.5f)
            {
                Camera.main.transform.position = new Vector3(shakePower, shakePower, Camera.main.transform.position.z);
            }
            else
            {
                Camera.main.transform.position = new Vector3(-shakePower, -shakePower, Camera.main.transform.position.z);
            }
        }
        else
        {
            shakePower = 0;
            Camera.main.transform.position = new Vector3(0, 0, Camera.main.transform.position.z);
        }
    }

}
