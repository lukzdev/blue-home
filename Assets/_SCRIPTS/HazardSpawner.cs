﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HazardSpawner : MonoBehaviour
{
    public GameManager gm;
    public List<HazardItem> hazardItems;
    private float totalWeights = 0;

    public GameObject firstHazard;

    private bool firstSpawn = true;

    public float initialSpawnDelay = 5;
    public int count = 0;

    public int maxCount = 100;

    public float spawnTimeSeconds = 10f;

    public Transform hazardsInfo;
    public BoxCollider2D viewBounds;

    public GameObject leftIndicator;
    public GameObject rightIndicator;

    private List<Hazard> hazards = new List<Hazard>();
    private List<ToggleHazard> toggleHazards = new List<ToggleHazard>();

    private float nextSpawn = 0;

    void Awake () 
    {
        nextSpawn = initialSpawnDelay;
    }

    void Start()
    {
        if (hazardItems.Count == 0)
        {
            Debug.LogError("No hazardItems set");
        }
        
        foreach (var item in hazardItems)
        {
            totalWeights += item.spawnWeight;
        }
        // spawn inital hazards
        Restart();
    }

    private GameObject GetRandomHazard()
    {
        float targetWeight = Random.Range(0, totalWeights);
        float weight = 0;
        foreach (var item in hazardItems)
        {
            weight += item.spawnWeight;
            if (weight < targetWeight) continue;

            return item.prefab;
        }

        return null;
    }


    void SpawnHazard (GameObject hazard)
    {
        if (hazard == null) return;

        bool onWater = (hazard.name == "OilRig" || hazard.name == "OilSpill");

        GameObject freeLandSpot = RandomFreeSpot(!onWater);

        if (freeLandSpot != null)
        {
            SpawnHazard(hazard, freeLandSpot);
        }
        // try to find one to toggle on instead
        //ToggleHazard pth = prefab.GetComponentInChildren<ToggleHazard>();
        //if (pth != null)
        //{
        //    foreach (var eth in toggleHazards)
        //    {
        //        if (eth.hazardName.Equals(pth.hazardName) && !eth.HazardEnabled) {
        //            eth.EnableHazard(true);
        //            return;
        //        }
        //    }
        //}
        // Debug.Log("Spawn " + prefab.name);

    }

    public GameObject RandomFreeSpot(bool isLand)
    {
        GameObject returnedSpot = null;
        GameObject[] spots;

        if (isLand)
        {
            spots = GameObject.FindGameObjectsWithTag("LandHazardSpot");
        }
        else
        {
            spots = GameObject.FindGameObjectsWithTag("WaterHazardSpot");
        }

        reshuffle(spots);

        for (int i = 0; i < spots.Length; i++)
        {
            if(spots[i].transform.childCount == 0)
            {
                returnedSpot = spots[i];
                break;
            }
        }

        return returnedSpot;
    }

    void reshuffle(GameObject[] texts)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < texts.Length; t++)
        {
            GameObject tmp = texts[t];
            int r = Random.Range(t, texts.Length);
            texts[t] = texts[r];
            texts[r] = tmp;
        }
    }


    void SpawnMeteorite(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        GameObject go = Instantiate(prefab, pos, rot, transform);
        Hazard hazard = go.GetComponentInChildren<Hazard>();
        hazards.Add(hazard);
        hazard.OnComplete += OnHazardComplete;
        hazard.OnFull += OnHazardFull;
        ToggleHazard th = go.GetComponentInChildren<ToggleHazard>();
        if (th != null)
        {
            toggleHazards.Add(th);
        }
    }

    void SpawnHazard(GameObject prefab, GameObject hazardSpot)
    {
        GameObject go = Instantiate(prefab, hazardSpot.transform);
        
        Hazard hazard = go.GetComponentInChildren<Hazard>();
        hazards.Add(hazard);
        hazard.OnComplete += OnHazardComplete;
        hazard.OnFull += OnHazardFull;
        ToggleHazard th = go.GetComponentInChildren<ToggleHazard>();
        if (th != null)
        {
            toggleHazards.Add(th);
        }
    }

    void OnHazardComplete(Hazard hazard)
    {
        gm.AddScore(hazard.extraScore);

        if (hazard.destroyOnComplate)
        {
            hazards.Remove(hazard);
            ToggleHazard th = hazard.gameObject.GetComponentInChildren<ToggleHazard>();
            if (th != null)
            {
                toggleHazards.Remove(th);
            }
        }
    }

    void OnHazardFull (Hazard hazard) 
    {
        gm.GameOver();
        foreach (var item in hazards)
        {
            item.Destroy();
        }
        hazards.Clear();
        toggleHazards.Clear();
        
        leftIndicator.SetActive(false);
        rightIndicator.SetActive(false);
        enabled = false;
    }

    void Update()
    {
        bool left = false;
        bool right = false;
        for (int i = hazards.Count - 1; i >= 0 ; i--)
        {
            Hazard hazard = hazards[i];
            if (hazard == null || hazard.gameObject == null)
            {
                Debug.Log("Removed");
                hazards.RemoveAt(i);
                continue;
            }
            if (hazard.CurrentProgress01 >= .5f && !viewBounds.OverlapPoint(hazard.transform.position)) {
                if (hazard.transform.position.x > hazardsInfo.transform.position.x) 
                {
                    right = true;
                }
                else
                {
                    left = true;
                }
            }
        }
        leftIndicator.SetActive(left);
        rightIndicator.SetActive(right);

        if (hazards.Count < maxCount)
        {
            nextSpawn -= Time.deltaTime;
            if (nextSpawn <= 0) {
                nextSpawn += spawnTimeSeconds;
                if (firstSpawn)
                {
                    firstSpawn = false;
                    SpawnMeteorite(firstHazard, transform.position, Quaternion.Euler(0, 0, 0));
                }
                else
                {
                    GameObject hazard = GetRandomHazard();

                    GameObject randomLand = RandomFreeSpot(true);
                    GameObject randomWater = RandomFreeSpot(false);

                    if ((randomLand == null || randomWater == null) || hazard.CompareTag("Meteor"))
                    {
                        SpawnMeteorite(firstHazard, transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
                    }
                    else
                    {
                        SpawnHazard(hazard);
                    }
                }
            }
        }
    }

    public void Restart () {
        enabled = true;
        firstSpawn = true;
        for (int i = 0; i < count; i++)
        {
            SpawnHazard(GetRandomHazard());
        }    
    }

    [System.Serializable]
    public class HazardItem {
        public GameObject prefab;
        public float spawnWeight;
    }
}
