﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedSprite : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;

    public Sprite[] sprites;
    public float frameTimeSeconds;
    private float frameTime;
    private int frame = 0;

    void Start()
    {
        frame = Random.Range(0, sprites.Length);
        spriteRenderer.sprite = sprites[frame];   
    }

    void Update()
    {
        frameTime += Time.deltaTime;
        if (frameTime >= frameTimeSeconds) 
        {
            frameTime -= frameTimeSeconds;
            frame = (frame + 1) % sprites.Length;
            spriteRenderer.sprite = sprites[frame];   
        }
    }
}
